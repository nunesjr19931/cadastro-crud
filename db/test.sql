DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`
(
    `id`    int(100) NOT NULL AUTO_INCREMENT,
    `name`  varchar(100) NOT NULL,
    `email` varchar(100) NOT NULL,
    `cpf`   varchar(100) NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3;