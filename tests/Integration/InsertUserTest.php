<?php

require_once __DIR__ . "/../IntegrationTestCase.php";

use Cadastro\User;
use Cadastro\UserRepository;
use PHPUnit\Framework\TestCase;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\DbUnit\DataSet\CsvDataSet;
use PHPUnit\DbUnit\DataSet\QueryDataSet;

class InsertUserTest extends Generic_Tests_DatabaseTestCase
{
    use TestCaseTrait;

    /** @var UserRepository  */
    protected $repository;

    protected $conn;

    public function setUp(): void
    {
        $this->conn = $this->getConn();
        $this->repository = new UserRepository($this->conn);
    }
    public function getDataSet()
    {
        $dataSet = $this->createCSVDataSet();
        return $dataSet;
    }

    protected function getConn()
    {
        $conn = new \PDO('mysql:host=mariadb;dbname=test', 'test', 'root');
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        return $conn;
    }

    public function tearDown(): void
    {
        $this->conn->query('TRUNCATE TABLE users');
    }

    public function testExample()
    {
        $this->assertTrue(true);
    }

    public function testInsertUser()
    {
        $nomeExpected = 'Nome Teste';
        $cpfExpected = '12345683000';
        $emailExpected = 'teste@teste.com';

        $dataSetExpected = new CsvDataSet();
        $dataSetExpected->addTable('users', dirname(__FILE__)."/../CsvExpected/usuarioTeste.csv");
        $dataSetExpected = $dataSetExpected->getTable('users');


        $dataSetActual = $this->getConnection()->createQueryTable('users','SELECT name, email, cpf FROM `users`' );

        $user = new User($this->repository, $nomeExpected, $cpfExpected, $emailExpected);

        $user->save();
        $this->assertTablesEqual($dataSetExpected,$dataSetActual);
    }


}