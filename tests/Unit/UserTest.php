<?php

use PHPUnit\Framework\TestCase;
use Cadastro\User;
use Cadastro\UserRepository;
use Prophecy\Prophecy\ObjectProphecy;


require_once('vendor/autoload.php');

class UserTest extends TestCase
{
    /** @var ObjectProphecy */
    private $repository;

    public function setUp(): void
    {
        $this->repository = $this->createMock(UserRepository::class);
    }

    public function testSaveUser()
    {
        $nomeExpected = 'Nome Teste';
        $cpfExpected = '12345683000';
        $emailExpected = 'teste@teste.com';

        $user = new User($this->repository, $nomeExpected, $cpfExpected, $emailExpected);

        $this->repository->expects($this->once())
            ->method('saveUser')
            ->with(
                $nomeExpected, 
                $cpfExpected, 
                $emailExpected
            )->willReturn(true);

        $user->save();
    }
}