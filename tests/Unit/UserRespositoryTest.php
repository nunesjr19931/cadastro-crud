<?php

use PHPUnit\Framework\TestCase;
use Cadastro\User;
use Cadastro\UserRepository;
use Prophecy\Argument;
use Prophecy\Prophecy\ObjectProphecy;


require_once('vendor/autoload.php');

class UserRespositoryTest extends TestCase
{
    /** @var ObjectProphecy|\PDO */
    private $conn;

    /** @var ObjectProphecy|UserRepository */
    private $repository;

    public function setUp(): void
    {
        $this->conn = $this->createMock(\PDO::class);
        $this->repository = $this->createMock(UserRepository::class);
    }

    public function testSaveUser()
    {
        $nomeExpected = 'Nome Teste';
        $cpfExpected = '12345683000';
        $emailExpected = 'teste@teste.com';

        $userRepository = new UserRepository($this->conn);

        $this->conn->expects($this->once())->method('query')->with(
            $this->logicalAnd(
                $this->stringContains('INSERT'),
                $this->stringContains($nomeExpected),
                $this->stringContains($cpfExpected),
                $this->stringContains($emailExpected)
            )
        );

        $userRepository->saveUser($nomeExpected, $emailExpected, $cpfExpected);

    }
}