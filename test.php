<?php

use Cadastro\User;
use Cadastro\UserRepository;

require_once('vendor/autoload.php');

$pdo = new \PDO('mysql:host=127.0.0.1;dbname=test', 'test', 'root');
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);


$userRepository = new UserRepository($pdo);
$user = new User($userRepository ,'Junior', '85269743000', 'junior@junior.com.br');
$user->save();