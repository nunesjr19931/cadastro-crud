<?php

namespace Cadastro;

class User
{
    /** @var string */
    private $name;

    /** @var string */
    private $cpf;

    /** @var string */
    private $email;

    /** @var UserRepository */
    private $repository;


    public function __construct($repository, $name, $cpf, $email)
    {
        $this->repository = $repository;
        $this->name = $name;
        $this->cpf = $cpf;
        $this->email = $email;
    }

    /**
     * Retorna o nome do usuario.
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Retorna o e-mail do usuario.
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Retorna o cpf do usuaio.
     * @return string
     */
    public function getCpf()
    {
        return $this->cpf;
    }

    /**
     * Persiste os dados do usuario no banco.
     */
    public function save()
    {
        $this->repository->saveUser(
            $this->name,
            $this->cpf, 
            $this->email
        );
    }

}